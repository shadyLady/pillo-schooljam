﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class Score : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private Text _scoreText; 

    [SerializeField] private int _score; 
    [SerializeField] private int[] _highscores; 

    private void Start()
    {
        ServiceLocator.Instance.GameManager.OnLevelEndEvent += Deactivate;

        _highscores = new int[5]; 

        for(int i = 0; i < _highscores.Length; i++)
        {
            _highscores[i] = PlayerPrefs.GetInt("HighScore" + i, 0);
        }
    }

    private void Update()
    {
        if (ServiceLocator.Instance.GameManager.GameState != GameState.STATE_GAME)
            return;

        float lastScore = _score; 

        if(_player.transform.position.y > lastScore)
        {
            _score = Mathf.RoundToInt(_player.transform.position.y);
            UpdateScores();
        }
    }

    private void UpdateScores()
    {
        if (_score < 10) _scoreText.text = "000" + _score;
        else if (_score < 100) _scoreText.text = "00" + _score;
        else if (_score < 1000) _scoreText.text = "0" + _score;
        else _scoreText.text = _score.ToString();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        for(int i = 0; i < _highscores.Length; i++)
        {
            if (_score > _highscores[i])
            {
                int temp = _highscores[i];

                PlayerPrefs.SetInt("HighScore" + i, _score);

                _score = temp;
            }
        }

        PlayerPrefs.Save();
    }

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Leaderboard : MonoBehaviour
{
    [SerializeField] private Text[] _highscoresText; 
    private int[] _highscores; 

    private void Awake()
    {
        _highscores = new int[5];

        for(int i = 0; i < _highscores.Length; i++)
        {
            string highscoreKey = "HighScore" + i;

            int highscore = PlayerPrefs.GetInt(highscoreKey, 0);

            _highscoresText[i].text = highscore.ToString();
        }
    }

}

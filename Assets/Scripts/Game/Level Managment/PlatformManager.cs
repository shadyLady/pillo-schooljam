﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void PlatformDelegate(GameObject platform);

public class PlatformManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> _platformPrefabs = new List<GameObject>(); 
    [SerializeField] private List<GameObject> _platformList = new List<GameObject>();

    [SerializeField] private float _minDistance;
    [SerializeField] private float _maxDistance;

    private Vector3 _highestPoint; 
    public Vector3 HighestPoint
    {
        get { return _highestPoint; }
    }

    private float _slack = 5.0f;

    private event PlatformDelegate _onPlatformRemoved; 
    public PlatformDelegate OnPlatformRemoved
    {
        get { return _onPlatformRemoved; }
        set { _onPlatformRemoved = value; }
    }

    private void Start()
    {
        ServiceLocator.Instance.PlatformManager = this;
        ReadyLevel();
    }
    private void Update()
    {
        CanSpawn();
        Border();
    }
    private void ReadyLevel()
    {
        ScreenBounds bounds = ServiceLocator.Instance.Utilities.Bounds();

        float distance = Random.Range(_maxDistance, _maxDistance);
        int amount = Mathf.RoundToInt((bounds.Height) / distance);

        for (int i = 0; i < amount; i++)
        {
            Vector3 position = new Vector3(Random.Range(bounds.Left + _slack, bounds.Right - _slack), ((bounds.Bottom + 10.0f) + (distance * i)));
            GameObject platform = Instantiate(_platformPrefabs[Random.Range(0, _platformPrefabs.Count)], position, Quaternion.identity) as GameObject;
            platform.name = "Platform " + i;
            _platformList.Add(platform);
        }

        _highestPoint = _platformList[_platformList.Count - 1].transform.position;
    }
    private void CanSpawn()
    {
        if (_platformList.Count > 0)
        {
            ScreenBounds bounds = ServiceLocator.Instance.Utilities.Bounds();

            if (_highestPoint.y < (bounds.Top - _minDistance))
            {
                Vector3 spawnPosition = new Vector3(Random.Range(bounds.Left + _slack, bounds.Right - _slack), _highestPoint.y + Random.Range(_minDistance, _maxDistance));

                _platformList.Add(Instantiate(_platformPrefabs[Random.Range(0, _platformPrefabs.Count)], spawnPosition, Quaternion.identity) as GameObject);
                _highestPoint = _platformList[_platformList.Count - 1].transform.position;
            }
        }
    }
    public List<GameObject> GetPlatforms()
    {
        return _platformList;
    }
    public GameObject GetPlatformByID(int id)
    {
        if (_platformList.Count > 0)
        {
            return _platformList[id];
        }
        else
        {
            Debug.Log("Oops! Platforms seem to be missing!");
            return null;
        }

    }
    public void RemovePlatform(GameObject platform)
    {
       if (_onPlatformRemoved != null)
            _onPlatformRemoved(_platformList[2]);

        Destroy(platform);
        _platformList.Remove(platform);
    }
    private void Border()
    {
        ScreenBounds bounds = ServiceLocator.Instance.Utilities.Bounds();

        for (int i = 0; i < _platformList.Count; i++)
        {
            Vector3 p = _platformList[i].transform.position;

            if (p.y < bounds.Bottom)
            {
                RemovePlatform(_platformList[i]);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Controller _target;
    [SerializeField]
    private Vector2 _charBoundsSize;

    [SerializeField]
    private float _verticalOffset;
    [SerializeField]
    private float _horizontalOffset;

    private CharacterBounds _characterBounds;

    private void Start()
    {
        _target = FindObjectOfType<Player>().GetComponent<Controller>();
        _characterBounds = new CharacterBounds(_target.GetComponent<Collider2D>().bounds, _charBoundsSize);
    }

    private void LateUpdate()
    {
        if (ServiceLocator.Instance.GameManager.GameState != GameState.STATE_GAME)
            return;

        _characterBounds.Update(_target.GetComponent<Collider2D>().bounds);

        Vector2 centerPosition = _characterBounds.FocusPoint + Vector2.up * _verticalOffset;
        centerPosition += Vector2.right * _horizontalOffset;

        transform.position = (Vector3)centerPosition + Vector3.forward * -47;
    }
}

public struct CharacterBounds
{
    public Vector2 FocusPoint;
    public Vector2 Velocity;

    private float _leftRestriction;
    private float _rightRestriction;
    private float _topRestriction;
    private float _bottomRestriction;

    public CharacterBounds(Bounds bounds, Vector2 viewPortSize) : this()
    {
        _leftRestriction = bounds.center.x - (viewPortSize.x * 0.5f);
        _rightRestriction = bounds.center.x + (viewPortSize.y * 0.5f);
        _bottomRestriction = bounds.min.y;
        _topRestriction = bounds.min.y + viewPortSize.y;

        Velocity = Vector3.zero;
    }

    public void Update(Bounds bounds)
    {
        float deltaX = 0;

        if (bounds.min.x < _leftRestriction)
            deltaX = bounds.min.x - _leftRestriction;
        else if (bounds.max.x > _rightRestriction)
            deltaX = bounds.max.x - _rightRestriction;

        _rightRestriction += deltaX;
        _leftRestriction += deltaX;

        float deltaY = 0;

        if (bounds.min.y < _bottomRestriction)
            deltaY = bounds.min.y - _bottomRestriction;
        else if (bounds.max.y > _topRestriction)
            deltaY = bounds.max.y - _topRestriction;

        _topRestriction += deltaY;
        _bottomRestriction += deltaY;

        FocusPoint = new Vector2((_leftRestriction + _rightRestriction) * 0.5f, (_topRestriction + _bottomRestriction) * 0.5f);
        Velocity = new Vector2(deltaX, deltaY);
    }
}


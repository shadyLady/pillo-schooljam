﻿using UnityEngine;
using System.Collections;

public class Cloud : RaycastData
{
    private bool _isDisappearing = false;

    [SerializeField] private float _fadeTime;
    [SerializeField] private float _fadeTimer = 0.0f;

    private bool _timerRunning = false;

    [SerializeField] private GameObject _powerUp;
    [SerializeField] private bool _isPowerUp = false;

    [SerializeField] [Range(0.1f, 1)] private float _fadeSpeed = 0.0f;
    private SpriteRenderer _renderer; 

    private GameObject _passenger;  

    protected override void Awake()
    {
		//SpawnPowerUp ();
        _verticalRaycastAmount = 8;
        _renderer = GetComponent<SpriteRenderer>();
        base.Awake();
    }

    private void Update()
    {
        if (ServiceLocator.Instance.GameManager.GameState != GameState.STATE_GAME)
            return;

        if (!_isDisappearing)
        {
            VerticalCollision();
        }
        else if (_isDisappearing)
        {
            if (!_timerRunning)
            {
                _timerRunning = true;

                _fadeTimer = _fadeTime;
            }
            else
            {
                FadeOut();
            }
        }
    }
    private void FadeOut()
    {
        if (_fadeTimer > 0.0f)
        {
            _fadeTimer -= _fadeSpeed * Time.deltaTime;
            float perc = (100 / _fadeTime) * _fadeTimer;
            _renderer.material.SetFloat("_Stage", (4.0f - (perc / 100) * 4));

        }
        else
        {
            _timerRunning = false;
            ServiceLocator.Instance.PlatformManager.RemovePlatform(this.gameObject);
        }
    }
	private void SpawnPowerUp()
    {
		if (Random.Range (0, 10) > 5)
        {
			GameObject PU = Instantiate(_powerUp, transform.position, Quaternion.identity) as GameObject;
			PU.transform.parent = gameObject.transform;
			_isPowerUp = true;
		}
	}
    private void VerticalCollision()
    {
        UpdateOriginPoints();

        float rayLength = 0.2f;

        for (int i = 0; i < _verticalRaycastAmount; i++)
        {
            Vector2 rayOrigin = RaycastOrigins.TopLeft;
            rayOrigin += Vector2.right * _verticalRayDistance * i;

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, _collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.up  * rayLength, Color.red);


            if (hit)
            {
                _isDisappearing = true; 
            }
        }
    }

}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public delegate void StateDelegate();

public enum GameState
{
    STATE_MENU,
    STATE_GAME,
    STATE_RESULT
};

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameState _gameState; 
    public GameState GameState
    {
        get { return _gameState; }
    }

    private event StateDelegate _onGameStartEvent; 
    public StateDelegate OnGameStartEvent
    {
        get { return _onGameStartEvent; }
        set { _onGameStartEvent = value; }
    }

    private event StateDelegate _onLevelStartEvent; 
    public StateDelegate OnLevelStartEvent
    {
        get { return _onLevelStartEvent; }
        set { _onLevelStartEvent = value; }
    }

    private event StateDelegate _onLevelEndEvent; 
    public StateDelegate OnLevelEndEvent
    {
        get { return _onLevelEndEvent; }
        set { _onLevelEndEvent = value; }
    }

    private void Start()
    {
        ServiceLocator.Instance.GameManager = this;

        _gameState = GameState.STATE_MENU;
    }
    public void StartGame()
    {
        if (_gameState != GameState.STATE_MENU)
            return;

        if (_onLevelStartEvent != null)
            _onLevelStartEvent();

        _gameState = GameState.STATE_GAME;
    }
    public void EndGame()
    {
        if (_gameState != GameState.STATE_GAME)
            return;

        if (_onLevelEndEvent != null)
            _onLevelEndEvent();

        _gameState = GameState.STATE_RESULT;
    }
    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

}

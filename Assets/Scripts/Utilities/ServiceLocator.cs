﻿using UnityEngine;
using System.Collections;

public class ServiceLocator : Singleton<ServiceLocator>
{
    private GameManager _gameManager; 
    public GameManager GameManager
    {
        get
        {
            if (_gameManager != null) return _gameManager;
            else { Debug.Log("Please assign game manager before calling it!"); return null; }
        }
        set { _gameManager = value; }
    }

    private Utilities _utilities; 
    public Utilities Utilities
    {
        get
        {
            if (_utilities != null) return _utilities;
            else { Debug.Log("Please assign utilities before calling them!"); return null; }
        }
        set { _utilities = value; }
    }

    private PlatformManager _platformManager; 
    public PlatformManager PlatformManager
    {
        get
        {
            if (_platformManager != null) return _platformManager;
            else { Debug.Log("Please assign platform manager before calling it!"); return null; }
        }
        set { _platformManager = value; }

    }
}

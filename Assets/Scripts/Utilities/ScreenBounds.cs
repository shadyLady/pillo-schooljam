﻿using UnityEngine;
using System.Collections;

public struct ScreenBounds 
{
    public float Top;
    public float Bottom;
    public float Left;
    public float Right;
    public float Height;
    public float Width;
}

﻿using UnityEngine;
using System.Collections;

public class Utilities : MonoBehaviour
{
    private void Start()
    {
        ServiceLocator.Instance.Utilities = this;
    }

    public ScreenBounds Bounds()
    {
        ScreenBounds b = new ScreenBounds();

        b.Top = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, Camera.main.transform.position.z)).y;
        b.Bottom = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)).y;
        b.Left = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)).x;
        b.Right = Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0.0f, Camera.main.transform.position.z)).x;

        b.Width = b.Right - b.Left;
        b.Height = b.Top - b.Bottom;

        return b;
    }
}

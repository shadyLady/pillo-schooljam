﻿using UnityEngine;
using System.Collections;


public class Controller : RaycastData
{
    private CollisionData _collisionData;
    public CollisionData CollisionData
    {
        get { return _collisionData; }
        set { _collisionData = value; }
    }

    private bool _canFallThrough; 
    public bool CanFallThrough
    {
        get { return _canFallThrough; }
        set
        {
            _canFallThrough = value;
            _collisionData.StandingOnPlatform = false;
            Invoke("ResetFallThrough", 1.0f);
        }
    }

    protected override void Awake()
    {
        base.Awake();

        _collisionData = new CollisionData();
    }

    public void Move(Vector3 velocity)
    {
        velocity *= Time.deltaTime;

        UpdateOriginPoints();
        _collisionData.ResetCollision();

        if (velocity.x != 0)
            velocity.x = HorizontalCollision(velocity);

        if (velocity.y != 0)
            velocity.y = VerticalCollision(velocity);

        transform.position += velocity;
    }
    private float HorizontalCollision(Vector3 velocity)
    {
        float horizontalDirection = Mathf.Sign(velocity.x);
        float rayLength = Mathf.Abs(velocity.x + _boundsOffset);

        for (int i = 0; i < _horizontalRaycastAmount; i++)
        {
            Vector2 rayOrigin = Vector2.zero;

            if (horizontalDirection == -1) { rayOrigin = RaycastOrigins.TopLeft; }
            else { rayOrigin = RaycastOrigins.TopRight; }

            rayOrigin += -Vector2.up * _horizontalRayDistance * i;

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * horizontalDirection, rayLength, _collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.right * horizontalDirection * rayLength, Color.red);

            if (hit)
            {
                if (hit.collider.tag == "Splash")
                    continue;

                if (hit.distance == 0)
                    continue;

                velocity.x = 0.0f;
                rayLength = hit.distance;

                _collisionData.LeftCollision = horizontalDirection == -1;
                _collisionData.RightCollision = horizontalDirection == 1;
            }
        }

        return velocity.x;
    }
    private float VerticalCollision(Vector3 velocity)
    {
        float verticalDirection = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y + _boundsOffset);

        for (int i = 0; i < _verticalRaycastAmount; i++)
        {
            Vector2 rayOrigin = Vector2.zero;

            if (verticalDirection == -1) { rayOrigin = RaycastOrigins.BottomLeft; }
            else { rayOrigin = RaycastOrigins.TopLeft; }

            rayOrigin += Vector2.right * _verticalRayDistance * i;

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * verticalDirection, rayLength, _collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.up * verticalDirection * rayLength, Color.red);


            if (hit)
            {
                if(hit.collider.tag == "One Way")
                {
                    if (verticalDirection == 1 || hit.distance == 0)
                        continue;
                }

                velocity.y = (hit.distance - _boundsOffset) * verticalDirection;
                rayLength = hit.distance;



                _collisionData.TopCollision = (verticalDirection == 1);
                _collisionData.BottomCollision = (verticalDirection == -1);
            }
        }

        return velocity.y;
    }
    private void ResetFallThrough()
    {
        _canFallThrough = false;
    }
}

public struct CollisionData
{
    public bool TopCollision;
    public bool BottomCollision;
    public bool LeftCollision;
    public bool RightCollision;
    public bool StandingOnPlatform;

    public void ResetCollision()
    {
        TopCollision = false;
        BottomCollision = false;
        LeftCollision = false;
        RightCollision = false;
        StandingOnPlatform = false;
    }
}

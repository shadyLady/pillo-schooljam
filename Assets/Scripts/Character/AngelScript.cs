﻿using UnityEngine;
using System.Collections;

public enum AngelState
{
    STATE_DRAGGING,
    STATE_HOLDING,
    STATE_MOVING
};

public class AngelScript : MonoBehaviour
{
	[SerializeField] private float _movespeed;
	[SerializeField] private Transform _selectedCloud;

    [SerializeField] private float _canvasWidth;

    [SerializeField] private AngelState _state;

    [SerializeField] private Vector3 _velocity;

    private Animator _animator; 

    private void Awake()
    {
        PilloController.ConfigureSensorRange(0x50, 0x6f);

        _animator = GetComponent<Animator>();
        _state = AngelState.STATE_MOVING;
    }

    private void Start()
    {
        ServiceLocator.Instance.PlatformManager.OnPlatformRemoved += SwitchTarget;
        ServiceLocator.Instance.GameManager.OnLevelStartEvent += FindFirstCloud;
    }

    private void FindFirstCloud()
    {
        _selectedCloud = ServiceLocator.Instance.PlatformManager.GetPlatformByID(1).transform;
    }
    private void SwitchTarget(GameObject newTarget)
    {
        _selectedCloud = newTarget.GetComponent<Transform>();
    }
	void ChangeCloudTransform(Transform preCloud)
    {
		Bounds bounds = preCloud.GetComponent<SpriteRenderer> ().bounds;
		transform.position = Vector3.MoveTowards (transform.position, new Vector3 (bounds.center.x, bounds.max.y), _movespeed / 30);

        float targetX = preCloud.position.x;
        float targetY = preCloud.position.y;

        Vector3 p = transform.position;

        if (p.x >= targetX - 0.2f && p.x <= targetX + 0.2f)
        {
            if (p.y >= targetY - 0.2f && p.y <= targetY + 0.2f)
            {
                if(_state != AngelState.STATE_HOLDING)
                {
                    _state = AngelState.STATE_HOLDING;
                    _animator.SetBool("Move", false);
                    _animator.SetBool("Drag", false);
                }

            }
        }
        else
        {
            if (_state != AngelState.STATE_MOVING)
            {
                _state = AngelState.STATE_MOVING;
                _animator.SetBool("Move", true);
            }
        }

    }

	private void Update ()
    {
        if (ServiceLocator.Instance.GameManager.GameState != GameState.STATE_GAME)
            return;

        float input = PilloController.GetSensor(Pillo.PilloID.Pillo2);

        float h = Input.GetAxisRaw("Horizontal");

		if(h > 0)
        {
            _velocity.x = _movespeed * Time.deltaTime;
        }
        else
        {
            _velocity.x = -_movespeed * Time.deltaTime;
        }

        if (_velocity.x != 0 && _state != AngelState.STATE_DRAGGING)
        {
            _state = AngelState.STATE_DRAGGING;
            _animator.SetBool("Move", false);
            _animator.SetBool("Drag", true);
        }
        else if (_velocity.x <= 0 && _state != AngelState.STATE_HOLDING)
        {
            _state = AngelState.STATE_HOLDING;
            //_animator.SetBool("Drag", false);
            _animator.SetBool("Move", false);
        }

        transform.position += _velocity;
        _selectedCloud.transform.position += _velocity;

        ChangeCloudTransform (_selectedCloud);

        Border();
    }

    private void Border()
    {
        Vector3 p = transform.position;
        Vector3 pC = _selectedCloud.transform.position;

        ScreenBounds bounds = ServiceLocator.Instance.Utilities.Bounds();

        if (p.x > bounds.Right - _canvasWidth) p.x = bounds.Right - _canvasWidth; _velocity.x = 0.0f;
        if (p.x < bounds.Left + _canvasWidth) p.x = bounds.Left + _canvasWidth; _velocity.x = 0.0f;

        if (pC.x > bounds.Right - _canvasWidth) pC.x = bounds.Right - _canvasWidth; _velocity.x = 0.0f;
        if (pC.x < bounds.Left + _canvasWidth) pC.x = bounds.Left + _canvasWidth; _velocity.x = 0.0f;

        transform.position = p;
        _selectedCloud.transform.position = pC;
    }
}

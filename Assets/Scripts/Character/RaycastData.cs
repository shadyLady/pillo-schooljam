﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class RaycastData : MonoBehaviour
{
    [SerializeField] protected LayerMask _collisionMask;

    protected int _horizontalRaycastAmount = 5;
    protected int _verticalRaycastAmount = 3;

    protected float _horizontalRayDistance;
    protected float _verticalRayDistance;

    protected float _boundsOffset = 0.0001f;

    private RaycastOrigins _raycastOrigins;
    public RaycastOrigins RaycastOrigins
    {
        get { return _raycastOrigins; }
    }

    private BoxCollider2D _collider;

    protected virtual void Awake()
    {
        InitializeOriginPoints();
    }

    protected void InitializeOriginPoints()
    {
        _raycastOrigins = new RaycastOrigins();
        _collider = GetComponent<BoxCollider2D>();

        Bounds bounds = _collider.bounds;
        bounds.Expand(_boundsOffset);

        _horizontalRayDistance = bounds.size.y / (_horizontalRaycastAmount - 1);
        _verticalRayDistance = bounds.size.x / (_verticalRaycastAmount - 1);

        UpdateOriginPoints();
    }

    protected void UpdateOriginPoints()
    {
        Bounds bounds = _collider.bounds;
        bounds.Expand(_boundsOffset * 2);

        _raycastOrigins.BottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        _raycastOrigins.TopRight = new Vector2(bounds.max.x, bounds.max.y);

        _raycastOrigins.TopLeft = new Vector2(bounds.min.x, bounds.max.y);
        _raycastOrigins.BottomRight = new Vector2(bounds.max.x, bounds.min.y);
    }
}

public struct RaycastOrigins
{
    public Vector2 TopLeft;
    public Vector2 TopRight;
    public Vector2 BottomLeft;
    public Vector2 BottomRight;
}

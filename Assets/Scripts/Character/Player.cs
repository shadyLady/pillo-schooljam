﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum PlayerState
{
    STATE_IDLE, 
    STATE_JUMP
};

public class Player : Controller
{
    [SerializeField] private float _gravity;
    [SerializeField] private float _jumpSpeed;
	[SerializeField] private float _jumpBoostCount;
	public float _JumpBoosCount{
		get{
			return _jumpBoostCount;
		}
		set{
			_jumpBoostCount = value;
		}
	}
	[SerializeField] private float _jumpBoostSpeed;

    [SerializeField] private GameObject _background;

    [SerializeField] private PlayerState _state;
    private bool _isFalling = false;

    private Vector3 _velocity;

    private Animator _animator; 

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _state = PlayerState.STATE_IDLE;

        GameObject spawnPlatform = ServiceLocator.Instance.PlatformManager.GetPlatformByID(0);
        Vector3 startPosition = new Vector3(spawnPlatform.transform.position.x, spawnPlatform.transform.position.y + 3.0f);
        transform.position = startPosition;

        ServiceLocator.Instance.GameManager.OnLevelStartEvent += GetToPosition;

    }

    private void GetToPosition()
    {

    }
    private void Update()
    {
        if (ServiceLocator.Instance.GameManager.GameState != GameState.STATE_GAME)
            return;

        Vector3 lastPos = transform.position;

        Move(_velocity);

        switch (_state)
        {
            case PlayerState.STATE_IDLE:
                {
                    float input = PilloController.GetSensor(Pillo.PilloID.Pillo1);
                    float v = Input.GetAxisRaw("Vertical");

                    if (CollisionData.BottomCollision)
                        _velocity.y = 0;

                    if (v > 0.2f && CollisionData.BottomCollision)
                    {
                        if (_jumpBoostCount > 0)
                        {
                            _velocity.y = _jumpSpeed - _jumpBoostSpeed;
                            _jumpBoostCount--;
                        }
                        else
                        {
                            _velocity.y = _jumpSpeed;
                            _animator.SetBool("Jump", true);
                        }

                        _state = PlayerState.STATE_JUMP;
                    }
                }
                break;
            case PlayerState.STATE_JUMP:
                {
                    if(_velocity.y < 0 && !_isFalling)
                    {
                        _isFalling = true; 
                    }

                    if(_isFalling && CollisionData.BottomCollision)
                    {
                        _animator.SetBool("Jump", false);
                        _animator.SetTrigger("Land");
                        _state = PlayerState.STATE_IDLE;
                    }
                }
                break;
        }

        float falldistance = Mathf.Abs(ServiceLocator.Instance.PlatformManager.HighestPoint.y - transform.position.y);

        if (falldistance > 20.0f)
            ServiceLocator.Instance.GameManager.EndGame();


        _velocity.y += _gravity;
        _background.GetComponent<SpriteRenderer>().material.SetFloat("_Height", transform.position.y);


    }
}

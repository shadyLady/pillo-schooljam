﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartWindow : MonoBehaviour
{
    private bool _playerOneReady = false;
    private bool _playerTwoReady = false;

    [SerializeField] private Image _playerOneSlider;
    [SerializeField] private Image _playerTwoSlider;

    private void Start()
    {
        ServiceLocator.Instance.GameManager.OnLevelStartEvent += Close;

        _playerOneSlider.material.SetFloat("_Score", 0);
        _playerTwoSlider.material.SetFloat("_Score", 0);
    }
    private void Update()
    {
        if(!_playerOneReady)
        {
            float P1 = PilloController.GetSensor(Pillo.PilloID.Pillo1);
            float K1 = Input.GetAxisRaw("Horizontal");

            float curValue = _playerOneSlider.material.GetFloat("_Score");

            if (K1 > 0)
            {
                if (curValue < 1.0f)
                {
                    curValue += Time.deltaTime;
                    _playerOneSlider.material.SetFloat("_Score", curValue);
                }
                else
                {
                    _playerOneReady = true;
                }
            }
            else
            {
                if(curValue > 0.0f)
                {
                    curValue -= Time.deltaTime;
                    _playerOneSlider.material.SetFloat("_Score", curValue);
                }
            }
        }

        if(!_playerTwoReady)
        {
            float P2 = PilloController.GetSensor(Pillo.PilloID.Pillo2);
            float K2 = Input.GetAxisRaw("Horizontal");

            float curValue = _playerTwoSlider.material.GetFloat("_Score");

            if (K2 > 0)
            {
                if (curValue < 1.0f)
                {
                    curValue += Time.deltaTime;
                    _playerTwoSlider.material.SetFloat("_Score", curValue);
                }
                else
                {
                    _playerTwoReady = true;
                }
            }
            else
            {
                if (curValue > 0.0f)
                {
                    curValue -= Time.deltaTime;
                    _playerTwoSlider.material.SetFloat("_Score", curValue);
                }
            }
        }

        if (_playerOneReady && _playerTwoReady)
            ServiceLocator.Instance.GameManager.StartGame();
    }
    private void Close()
    {
        gameObject.SetActive(false);
    }

}

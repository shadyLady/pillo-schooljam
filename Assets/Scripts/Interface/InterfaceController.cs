﻿using UnityEngine;
using System.Collections;

public class InterfaceController : MonoBehaviour
{
    [SerializeField] private GameObject _resultWindow; 
    
    private void Start()
    {
        ServiceLocator.Instance.GameManager.OnLevelEndEvent += OpenResult;
    }
    private void OpenResult()
    {
        _resultWindow.SetActive(true);
    }
    private void CloseResult()
    {
        _resultWindow.SetActive(true);
    }
}

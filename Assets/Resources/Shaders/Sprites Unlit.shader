// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:34573,y:32746,varname:node_1873,prsc:2|emission-6010-OUT,alpha-1077-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32249,y:32863,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1086,x:33015,y:32820,cmnt:RGB,varname:node_1086,prsc:2|A-426-OUT,B-5983-RGB,C-5376-RGB;n:type:ShaderForge.SFN_Color,id:5983,x:32670,y:32839,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32572,y:33093,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:33261,y:32820,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:33015,y:32987,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5983-A,C-5376-A;n:type:ShaderForge.SFN_RgbToHsv,id:161,x:32487,y:32737,varname:node_161,prsc:2|IN-4805-RGB;n:type:ShaderForge.SFN_HsvToRgb,id:426,x:32829,y:32719,varname:node_426,prsc:2|H-6602-OUT,S-161-SOUT,V-161-VOUT;n:type:ShaderForge.SFN_Add,id:6602,x:32656,y:32627,varname:node_6602,prsc:2|A-204-OUT,B-161-HOUT;n:type:ShaderForge.SFN_Slider,id:204,x:32292,y:32605,ptovrint:False,ptlb:Hue,ptin:_Hue,varname:_Hue,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:474,x:33246,y:33114,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:_Opacity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:1077,x:33618,y:33030,varname:node_1077,prsc:2|A-603-OUT,B-474-OUT;n:type:ShaderForge.SFN_TexCoord,id:9951,x:31445,y:33721,varname:node_9951,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:430,x:31605,y:33481,ptovrint:False,ptlb:Score,ptin:_Score,varname:_Score,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_ComponentMask,id:1642,x:31617,y:33738,varname:node_1642,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9951-U;n:type:ShaderForge.SFN_Color,id:9568,x:33599,y:33305,ptovrint:False,ptlb:Color Player 1,ptin:_ColorPlayer1,varname:_ColorPlayer1,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.0896554,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:2927,x:33599,y:33478,ptovrint:False,ptlb:Color Player 2,ptin:_ColorPlayer2,varname:_ColorPlayer2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:8731,x:33857,y:33478,varname:node_8731,prsc:2|A-9568-RGB,B-2927-RGB,T-7456-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6010,x:34293,y:32843,ptovrint:False,ptlb:WindBar,ptin:_WindBar,varname:_ScoreBar,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-1749-OUT,B-9826-OUT;n:type:ShaderForge.SFN_Multiply,id:9826,x:34101,y:32905,varname:node_9826,prsc:2|A-1749-OUT,B-8731-OUT;n:type:ShaderForge.SFN_Clamp01,id:7366,x:32857,y:34127,varname:node_7366,prsc:2|IN-6636-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:573,x:32210,y:33899,varname:node_573,prsc:2|IN-430-OUT,IMIN-9256-OUT,IMAX-3315-OUT,OMIN-4191-OUT,OMAX-3058-OUT;n:type:ShaderForge.SFN_Vector1,id:9256,x:31832,y:33708,varname:node_9256,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:3315,x:31832,y:33782,varname:node_3315,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:7211,x:31476,y:34245,varname:node_7211,prsc:2,v1:0.5;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:6030,x:32170,y:33754,varname:node_6030,prsc:2|IN-1642-OUT,IMIN-9256-OUT,IMAX-3315-OUT,OMIN-4191-OUT,OMAX-3058-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3709,x:31476,y:34087,ptovrint:False,ptlb:Sharpness,ptin:_Sharpness,varname:_Sharpness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_Negate,id:4191,x:31693,y:34087,varname:node_4191,prsc:2|IN-3709-OUT;n:type:ShaderForge.SFN_Add,id:3058,x:31693,y:34226,varname:node_3058,prsc:2|A-3709-OUT,B-7211-OUT;n:type:ShaderForge.SFN_Step,id:4893,x:32404,y:33873,varname:node_4893,prsc:2|A-1642-OUT,B-2210-OUT;n:type:ShaderForge.SFN_Vector1,id:2210,x:32231,y:34054,varname:node_2210,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Clamp01,id:5702,x:32857,y:33873,varname:node_5702,prsc:2|IN-3729-OUT;n:type:ShaderForge.SFN_OneMinus,id:3729,x:32638,y:33873,varname:node_3729,prsc:2|IN-4893-OUT;n:type:ShaderForge.SFN_Subtract,id:2211,x:33092,y:33977,varname:node_2211,prsc:2|A-7366-OUT,B-7751-OUT;n:type:ShaderForge.SFN_Clamp01,id:3168,x:33284,y:33977,varname:node_3168,prsc:2|IN-2211-OUT;n:type:ShaderForge.SFN_If,id:7456,x:33599,y:33631,varname:node_7456,prsc:2|A-430-OUT,B-6259-OUT,GT-3168-OUT,EQ-6193-OUT,LT-7257-OUT;n:type:ShaderForge.SFN_Vector1,id:6259,x:33377,y:33653,varname:node_6259,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:6193,x:33377,y:33707,varname:node_6193,prsc:2,v1:0;n:type:ShaderForge.SFN_OneMinus,id:1458,x:32147,y:33571,varname:node_1458,prsc:2|IN-430-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:3444,x:32404,y:33678,varname:node_3444,prsc:2|IN-1458-OUT,IMIN-9256-OUT,IMAX-3315-OUT,OMIN-4191-OUT,OMAX-3058-OUT;n:type:ShaderForge.SFN_Add,id:6570,x:32638,y:33743,varname:node_6570,prsc:2|A-6030-OUT,B-3444-OUT;n:type:ShaderForge.SFN_Clamp01,id:1505,x:32857,y:33743,varname:node_1505,prsc:2|IN-6570-OUT;n:type:ShaderForge.SFN_Subtract,id:4823,x:33092,y:33743,varname:node_4823,prsc:2|A-1505-OUT,B-5702-OUT;n:type:ShaderForge.SFN_Clamp01,id:7257,x:33260,y:33755,varname:node_7257,prsc:2|IN-4823-OUT;n:type:ShaderForge.SFN_Clamp01,id:7751,x:32638,y:34001,varname:node_7751,prsc:2|IN-4893-OUT;n:type:ShaderForge.SFN_OneMinus,id:3719,x:32126,y:34243,varname:node_3719,prsc:2|IN-1642-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:4278,x:32377,y:34243,varname:node_4278,prsc:2|IN-3719-OUT,IMIN-9256-OUT,IMAX-3315-OUT,OMIN-4191-OUT,OMAX-3058-OUT;n:type:ShaderForge.SFN_Add,id:6636,x:32638,y:34127,varname:node_6636,prsc:2|A-573-OUT,B-4278-OUT;proporder:4805-5983-204-474-6010-430-9568-2927-3709;pass:END;sub:END;*/

Shader "Shader Forge/Sprites Unlit" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Hue ("Hue", Range(0, 1)) = 0
        _Opacity ("Opacity", Range(0, 1)) = 1
        [MaterialToggle] _WindBar ("WindBar", Float ) = 0
        _Score ("Score", Range(0, 1)) = 0
        _ColorPlayer1 ("Color Player 1", Color) = (0,0.0896554,1,1)
        _ColorPlayer2 ("Color Player 2", Color) = (1,0,0,1)
        _Sharpness ("Sharpness", Float ) = 10
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float _Hue;
            uniform float _Opacity;
            uniform float _Score;
            uniform float4 _ColorPlayer1;
            uniform float4 _ColorPlayer2;
            uniform fixed _WindBar;
            uniform float _Sharpness;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_161_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_161_p = lerp(float4(float4(_MainTex_var.rgb,0.0).zy, node_161_k.wz), float4(float4(_MainTex_var.rgb,0.0).yz, node_161_k.xy), step(float4(_MainTex_var.rgb,0.0).z, float4(_MainTex_var.rgb,0.0).y));
                float4 node_161_q = lerp(float4(node_161_p.xyw, float4(_MainTex_var.rgb,0.0).x), float4(float4(_MainTex_var.rgb,0.0).x, node_161_p.yzx), step(node_161_p.x, float4(_MainTex_var.rgb,0.0).x));
                float node_161_d = node_161_q.x - min(node_161_q.w, node_161_q.y);
                float node_161_e = 1.0e-10;
                float3 node_161 = float3(abs(node_161_q.z + (node_161_q.w - node_161_q.y) / (6.0 * node_161_d + node_161_e)), node_161_d / (node_161_q.x + node_161_e), node_161_q.x);;
                float node_603 = (_MainTex_var.a*_Color.a*i.vertexColor.a); // A
                float3 node_1749 = (((lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac((_Hue+node_161.r)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_161.g)*node_161.b)*_Color.rgb*i.vertexColor.rgb)*node_603); // Premultiply Alpha
                float node_7456_if_leA = step(_Score,0.5);
                float node_7456_if_leB = step(0.5,_Score);
                float node_1642 = i.uv0.r.r;
                float node_9256 = 0.0;
                float node_3315 = 1.0;
                float node_4191 = (-1*_Sharpness);
                float node_3058 = (_Sharpness+0.5);
                float node_4893 = step(node_1642,0.5);
                float3 emissive = lerp( node_1749, (node_1749*lerp(_ColorPlayer1.rgb,_ColorPlayer2.rgb,lerp((node_7456_if_leA*saturate((saturate(((node_4191 + ( (node_1642 - node_9256) * (node_3058 - node_4191) ) / (node_3315 - node_9256))+(node_4191 + ( ((1.0 - _Score) - node_9256) * (node_3058 - node_4191) ) / (node_3315 - node_9256))))-saturate((1.0 - node_4893)))))+(node_7456_if_leB*saturate((saturate(((node_4191 + ( (_Score - node_9256) * (node_3058 - node_4191) ) / (node_3315 - node_9256))+(node_4191 + ( ((1.0 - node_1642) - node_9256) * (node_3058 - node_4191) ) / (node_3315 - node_9256))))-saturate(node_4893)))),0.0,node_7456_if_leA*node_7456_if_leB))), _WindBar );
                float3 finalColor = emissive;
                return fixed4(finalColor,(node_603*_Opacity));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

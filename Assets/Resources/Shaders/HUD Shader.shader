// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33433,y:32719,varname:node_1873,prsc:2|emission-1086-OUT,alpha-3534-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32099,y:32720,varname:_MainTex_copy,prsc:2,tex:e5acee21c2efab94ca8728eebe65a123,ntxv:0,isnm:False|UVIN-6285-UVOUT,TEX-8140-TEX;n:type:ShaderForge.SFN_Multiply,id:1086,x:32832,y:32797,cmnt:RGB,varname:node_1086,prsc:2|A-3409-OUT,B-5983-RGB,C-5376-RGB;n:type:ShaderForge.SFN_Color,id:5983,x:32447,y:32682,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32544,y:32886,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:33069,y:32797,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:32944,y:32988,cmnt:A,varname:node_603,prsc:2|A-5376-A,B-97-OUT;n:type:ShaderForge.SFN_TexCoord,id:5230,x:31344,y:32561,varname:node_5230,prsc:2,uv:0;n:type:ShaderForge.SFN_UVTile,id:6285,x:31688,y:32682,varname:node_6285,prsc:2|UVIN-5230-UVOUT,WDT-6236-OUT,HGT-6236-OUT,TILE-5627-OUT;n:type:ShaderForge.SFN_Vector1,id:6236,x:31335,y:32718,varname:node_6236,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:3309,x:31140,y:32967,varname:node_3309,prsc:2|A-2589-OUT,B-3092-OUT;n:type:ShaderForge.SFN_Vector1,id:3092,x:30895,y:33061,varname:node_3092,prsc:2,v1:1;n:type:ShaderForge.SFN_UVTile,id:6075,x:31688,y:32865,varname:node_6075,prsc:2|UVIN-5230-UVOUT,WDT-6236-OUT,HGT-6236-OUT,TILE-8893-OUT;n:type:ShaderForge.SFN_Tex2d,id:3657,x:32082,y:32858,varname:node_3657,prsc:2,tex:e5acee21c2efab94ca8728eebe65a123,ntxv:0,isnm:False|UVIN-6075-UVOUT,TEX-8140-TEX;n:type:ShaderForge.SFN_Lerp,id:3409,x:32312,y:32886,varname:node_3409,prsc:2|A-4805-RGB,B-3657-RGB,T-7928-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:8140,x:31855,y:32770,ptovrint:False,ptlb:SpriteSheet,ptin:_SpriteSheet,varname:_SpriteSheet,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e5acee21c2efab94ca8728eebe65a123,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Trunc,id:2589,x:30895,y:32924,varname:node_2589,prsc:2|IN-2230-OUT;n:type:ShaderForge.SFN_Frac,id:7928,x:31632,y:33155,varname:node_7928,prsc:2|IN-2230-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:8893,x:31335,y:32978,varname:node_8893,prsc:2,min:0,max:3|IN-3309-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:5627,x:31335,y:32818,varname:node_5627,prsc:2,min:0,max:3|IN-2589-OUT;n:type:ShaderForge.SFN_Slider,id:2230,x:30458,y:33146,ptovrint:False,ptlb:Stage,ptin:_Stage,varname:_Stage,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:4,max:4;n:type:ShaderForge.SFN_Tex2d,id:5593,x:32447,y:32468,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3534,x:33150,y:33069,varname:node_3534,prsc:2|A-603-OUT,B-3623-OUT;n:type:ShaderForge.SFN_Slider,id:3623,x:32772,y:33282,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_3623,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:928,x:32312,y:33128,varname:node_928,prsc:2|A-4805-A,B-3657-A,T-7928-OUT;n:type:ShaderForge.SFN_Clamp01,id:97,x:32721,y:33090,varname:node_97,prsc:2|IN-9779-OUT;n:type:ShaderForge.SFN_Multiply,id:9779,x:32533,y:33096,varname:node_9779,prsc:2|A-5593-A,B-928-OUT;proporder:5983-8140-2230-5593-3623;pass:END;sub:END;*/

Shader "Shader Forge/HUD Shader" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _SpriteSheet ("SpriteSheet", 2D) = "white" {}
        _Stage ("Stage", Range(0, 4)) = 4
        _MainTex ("MainTex", 2D) = "white" {}
        _Opacity ("Opacity", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _SpriteSheet; uniform float4 _SpriteSheet_ST;
            uniform float _Stage;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Opacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float node_6236 = 2.0;
                float node_2589 = trunc(_Stage);
                float node_5627 = clamp(node_2589,0,3);
                float2 node_6285_tc_rcp = float2(1.0,1.0)/float2( node_6236, node_6236 );
                float node_6285_ty = floor(node_5627 * node_6285_tc_rcp.x);
                float node_6285_tx = node_5627 - node_6236 * node_6285_ty;
                float2 node_6285 = (i.uv0 + float2(node_6285_tx, node_6285_ty)) * node_6285_tc_rcp;
                float4 _MainTex_copy = tex2D(_SpriteSheet,TRANSFORM_TEX(node_6285, _SpriteSheet));
                float node_8893 = clamp((node_2589+1.0),0,3);
                float2 node_6075_tc_rcp = float2(1.0,1.0)/float2( node_6236, node_6236 );
                float node_6075_ty = floor(node_8893 * node_6075_tc_rcp.x);
                float node_6075_tx = node_8893 - node_6236 * node_6075_ty;
                float2 node_6075 = (i.uv0 + float2(node_6075_tx, node_6075_ty)) * node_6075_tc_rcp;
                float4 node_3657 = tex2D(_SpriteSheet,TRANSFORM_TEX(node_6075, _SpriteSheet));
                float node_7928 = frac(_Stage);
                float3 node_1086 = (lerp(_MainTex_copy.rgb,node_3657.rgb,node_7928)*_Color.rgb*i.vertexColor.rgb); // RGB
                float3 emissive = node_1086;
                float3 finalColor = emissive;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_603 = (i.vertexColor.a*saturate((_MainTex_var.a*lerp(_MainTex_copy.a,node_3657.a,node_7928)))); // A
                return fixed4(finalColor,(node_603*_Opacity));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33918,y:33013,varname:node_1873,prsc:2|emission-720-OUT,alpha-7671-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:31841,y:33259,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:5983,x:32267,y:33467,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32346,y:33292,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:603,x:33028,y:33259,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5983-A,C-5376-A;n:type:ShaderForge.SFN_Multiply,id:2339,x:32938,y:33091,cmnt:RGB,varname:node_2339,prsc:2|A-1630-OUT,B-5376-RGB,C-5983-RGB;n:type:ShaderForge.SFN_RgbToHsv,id:1018,x:32048,y:33181,varname:node_1018,prsc:2|IN-4805-RGB;n:type:ShaderForge.SFN_HsvToRgb,id:1630,x:32450,y:33149,varname:node_1630,prsc:2|H-9511-OUT,S-1018-SOUT,V-1018-VOUT;n:type:ShaderForge.SFN_Add,id:9511,x:32220,y:33053,varname:node_9511,prsc:2|A-5256-OUT,B-1018-HOUT;n:type:ShaderForge.SFN_Slider,id:5256,x:31853,y:33049,ptovrint:False,ptlb:Hue,ptin:_Hue,varname:_Hue,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:9589,x:32916,y:33411,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:_Opacity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:7671,x:33372,y:33245,varname:node_7671,prsc:2|A-603-OUT,B-9589-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:8595,x:31587,y:34140,ptovrint:False,ptlb:Vertical,ptin:_Vertical,varname:node_8595,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-5890-OUT,B-6515-OUT;n:type:ShaderForge.SFN_TexCoord,id:7797,x:31084,y:34184,varname:node_7797,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:9582,x:31457,y:33849,ptovrint:False,ptlb:Score,ptin:_Score,varname:_Score,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9024501,max:1;n:type:ShaderForge.SFN_ComponentMask,id:5890,x:31334,y:34211,varname:node_5890,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7797-U;n:type:ShaderForge.SFN_Color,id:2860,x:32427,y:33830,ptovrint:False,ptlb:Color Player 1,ptin:_ColorPlayer1,varname:_ColorPlayer1,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.0896554,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:3456,x:32438,y:34010,ptovrint:False,ptlb:Color Player 2,ptin:_ColorPlayer2,varname:_ColorPlayer2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:4083,x:32925,y:34118,varname:node_4083,prsc:2|A-2860-RGB,B-3456-RGB,T-3644-OUT;n:type:ShaderForge.SFN_Vector1,id:7866,x:31587,y:34295,varname:node_7866,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:4908,x:31500,y:34424,varname:node_4908,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:9429,x:31261,y:34747,varname:node_9429,prsc:2,v1:0.5;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:7099,x:32057,y:34275,varname:node_7099,prsc:2|IN-8595-OUT,IMIN-7866-OUT,IMAX-4908-OUT,OMIN-4670-OUT,OMAX-4558-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9875,x:31261,y:34679,ptovrint:False,ptlb:Sharpness,ptin:_Sharpness,varname:_Sharpness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_Negate,id:4670,x:31500,y:34584,varname:node_4670,prsc:2|IN-9875-OUT;n:type:ShaderForge.SFN_Add,id:4558,x:31500,y:34735,varname:node_4558,prsc:2|A-9875-OUT,B-9429-OUT;n:type:ShaderForge.SFN_OneMinus,id:8139,x:31954,y:34048,varname:node_8139,prsc:2|IN-9582-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:6312,x:32203,y:34147,varname:node_6312,prsc:2|IN-8139-OUT,IMIN-7866-OUT,IMAX-4908-OUT,OMIN-4670-OUT,OMAX-4558-OUT;n:type:ShaderForge.SFN_Add,id:1621,x:32445,y:34220,varname:node_1621,prsc:2|A-7099-OUT,B-6312-OUT;n:type:ShaderForge.SFN_Clamp01,id:3644,x:32664,y:34220,varname:node_3644,prsc:2|IN-1621-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6515,x:31334,y:34350,varname:node_6515,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7797-V;n:type:ShaderForge.SFN_Multiply,id:720,x:33565,y:33113,varname:node_720,prsc:2|A-2339-OUT,B-4503-OUT;n:type:ShaderForge.SFN_Lerp,id:4503,x:33048,y:33832,varname:node_4503,prsc:2|A-4083-OUT,B-8894-RGB,T-6527-OUT;n:type:ShaderForge.SFN_Trunc,id:6527,x:32377,y:33655,varname:node_6527,prsc:2|IN-9582-OUT;n:type:ShaderForge.SFN_Color,id:8894,x:32739,y:33701,ptovrint:False,ptlb:node_8894,ptin:_node_8894,varname:node_8894,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:4805-5983-5256-9589-8595-9582-2860-3456-9875-8894;pass:END;sub:END;*/

Shader "Shader Forge/ChargeBar Shader" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Hue ("Hue", Range(0, 1)) = 0
        _Opacity ("Opacity", Range(0, 1)) = 1
        [MaterialToggle] _Vertical ("Vertical", Float ) = 0
        _Score ("Score", Range(0, 1)) = 0.9024501
        _ColorPlayer1 ("Color Player 1", Color) = (0,0.0896554,1,1)
        _ColorPlayer2 ("Color Player 2", Color) = (1,0,0,1)
        _Sharpness ("Sharpness", Float ) = 10
        _node_8894 ("node_8894", Color) = (0.5,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float _Hue;
            uniform float _Opacity;
            uniform fixed _Vertical;
            uniform float _Score;
            uniform float4 _ColorPlayer1;
            uniform float4 _ColorPlayer2;
            uniform float _Sharpness;
            uniform float4 _node_8894;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_1018_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_1018_p = lerp(float4(float4(_MainTex_var.rgb,0.0).zy, node_1018_k.wz), float4(float4(_MainTex_var.rgb,0.0).yz, node_1018_k.xy), step(float4(_MainTex_var.rgb,0.0).z, float4(_MainTex_var.rgb,0.0).y));
                float4 node_1018_q = lerp(float4(node_1018_p.xyw, float4(_MainTex_var.rgb,0.0).x), float4(float4(_MainTex_var.rgb,0.0).x, node_1018_p.yzx), step(node_1018_p.x, float4(_MainTex_var.rgb,0.0).x));
                float node_1018_d = node_1018_q.x - min(node_1018_q.w, node_1018_q.y);
                float node_1018_e = 1.0e-10;
                float3 node_1018 = float3(abs(node_1018_q.z + (node_1018_q.w - node_1018_q.y) / (6.0 * node_1018_d + node_1018_e)), node_1018_d / (node_1018_q.x + node_1018_e), node_1018_q.x);;
                float node_7866 = 0.0;
                float node_4908 = 1.0;
                float node_4670 = (-1*_Sharpness);
                float node_4558 = (_Sharpness+0.5);
                float3 emissive = (((lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac((_Hue+node_1018.r)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_1018.g)*node_1018.b)*i.vertexColor.rgb*_Color.rgb)*lerp(lerp(_ColorPlayer1.rgb,_ColorPlayer2.rgb,saturate(((node_4670 + ( (lerp( i.uv0.r.r, i.uv0.g.r, _Vertical ) - node_7866) * (node_4558 - node_4670) ) / (node_4908 - node_7866))+(node_4670 + ( ((1.0 - _Score) - node_7866) * (node_4558 - node_4670) ) / (node_4908 - node_7866))))),_node_8894.rgb,trunc(_Score)));
                float3 finalColor = emissive;
                return fixed4(finalColor,((_MainTex_var.a*_Color.a*i.vertexColor.a)*_Opacity));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
